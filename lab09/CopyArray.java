///Luke Christman lab09
////////////////

public class CopyArray{
  
  public static int[] copy( int[] list ){
    int[] list2;
    list2 = new int[list.length];//
    for (int i=0; i<list2.length; i++){
      list2[i] = list[i];
    }
    return list2;//making a copy of the array that is passed in
  }
  
    public static void inverter(int[] array){
    int temp = 0;
    for(int k = 0; k < array.length/2 ; k++){
      temp = array[array.length - k - 1];
      array[array.length - k - 1] = array[k];
      array[k] = temp;
    }
    }//inverting the array that is passed into the method
  
  public static int[] inverter2(int[] list){
    int[] invertedArray;
    invertedArray = new int[list.length];
    invertedArray = copy(list);
    inverter(invertedArray);
    return invertedArray;
  }//inverting the array by passing it thriohugh the copy and inverter method
  
  public static void print(int[] list){
    for (int i = 0; i<list.length; i++){
      System.out.print(list[i]+" ");
    }
  }//prints the array
  
  public static void main(String arg[]){
    int[] array0 = { 2, 4, 6, 8, 10, 12, 14, 16 };
    int[] array1 = copy(array0);//copying the original array twice
    int[] array2 = copy(array0);
    
     
    inverter(array0);
    print(array0);
    System.out.println();//prints an inverted array because it is passed directly into inverter
    
    inverter2(array1);
    print(array1);
    System.out.println();//prints the original array because the copy is passed into inverter
    
    int[] array3 = inverter2(array2);
    print(array3);
    System.out.println();//prints the inverted array because array3 is set equal to array2 when it is passed through the method
  }// end of main method
}//end of class


  
  
  
  