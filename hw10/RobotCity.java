/////////////
///CSE 2 RobotCity
///Luke Christman
///2-20-2018
///This propgram will:
///Build a 2d array with a random number of member arrays and members, randomly make certain
///values of the array negative, then move these negative signs to the right.
/////////////

public class RobotCity{
  
  public static int[][] buildCity(){//method that returns an array of integers
    int[][] cityArray = new int[(int)(Math.random()*6 + 10)][(int)(Math.random()*6 + 10)];//declares 2D array and allocates it 10-15 member arrays with 10-15 members each
    for (int i = 0; i<cityArray.length; i++){//for loop to access each member array
			for(int j = 0; j<cityArray[i].length; j++){//for loop to access each member within each member array
        cityArray[i][j] = (int)(Math.random()*900 + 100); //for every single value within the 2D array, it will have a value between 100 and 999       
      }     
    }
    return cityArray;//return the array
  }
  
  public static void display(int[][] cityPopulation){//method that prints out the array in a grid like format
    for (int i = 0; i<cityPopulation.length; i++){
			for(int j = 0; j<cityPopulation[i].length; j++){
        System.out.printf(" % 5d", cityPopulation[i][j]);//prints out each value of with at least 5 place values       
      }
      System.out.println();//skip a line and move to the next member array
    }
  }
  
  public static int[][] invade(int [][] cityPopulation, int k){//method accepts an array and an integer
    for (int i = 0; i<k; i++){//for the number of "robots in the city"
      int randomBlockRow = (int)(Math.random()*(cityPopulation.length));//create a random value that is less or equal too than the number of member array
      int randomBlockColumn = (int)(Math.random()*(cityPopulation[0].length));//create a random value that is less or equal too than the number of members in a member array
      if (cityPopulation[randomBlockRow][randomBlockColumn] > 0){//if the value of that member of that member array has not already been turned negative:
        cityPopulation[randomBlockRow][randomBlockColumn] = -1*(cityPopulation[randomBlockRow][randomBlockColumn]);//making that value of the array negative
      }
      else{//if the value is already negative
        i--;//subtract 1 from i so that this "robot is not skipped". the loop will run again for that same value of i
      }
    }
    return cityPopulation;//return the array with negative values in it
  }
  
  public static int[][] update(int[][] cityPopulation){//method accepts and returns a 2d array
      for(int i = 0; i < cityPopulation.length; i++){  //for loop hits each column of the 2d array
        for(int j = cityPopulation[i].length - 1; j > 0; j--){ //moves right to left through the row of the 2d array
          if (cityPopulation[i][j] < 0 && cityPopulation[i][j-1] < 0){//if there are negative values back to back:
            cityPopulation[i][j-1] = cityPopulation[i][j-1]*(-1);//make the value to the left positve 
            j--;//subtract 1 from j in order to skip the next value so that the same value is not multiplied by -1 twice
          }
          else{//if there is no negative back to back
            if(cityPopulation[i][j] < 0){//if current term is negative
              cityPopulation[i][j] = cityPopulation[i][j]*(-1);//make it positive
            }
            if(cityPopulation[i][j-1] < 0){ //and if the term to the left is negative:
              cityPopulation[i][j] = cityPopulation[i][j]*(-1);//set the current value to negative 
            } 
          }
        }
        //the first column of the array is inaccessible because it is out of bounds, so we need to address it seperately
          if( cityPopulation[i][0] < 0){//if the value in the first column is negative
            cityPopulation[i][0] = cityPopulation[i][0]*(-1);//make it positive
          }
        
      }
    return cityPopulation;//return the array with shifted negative signs
  }
  
  public static void main(String[] args){
    int[][] newCity;//declare an array
    newCity = buildCity();//pass the array through this method
    display(newCity);//print the array
    System.out.println("-----------------------------------------------------------------------------");
    int numberOfRobots = (int)(Math.random()*25);//create a random number of robots
    int[][] invadedCity;//declare a new array
    invadedCity = invade(newCity, numberOfRobots);//pass the previously built array through the invade method and assign it to a new array
    display(invadedCity);//print the new array after invasion
    System.out.println("-----------------------------------------------------------------------------");
    int[][] updatedCity;//declare a new array
    int counter = 0;//declare counter outside of the while loop
    while (counter < 5){//run loop 5 times
      updatedCity = update(invadedCity);//run the invaded city array through this method
      display(updatedCity);//print the array
      counter++;//add one to the counter
      System.out.println();//skip a line
    }
  }
  
  
}
  
  