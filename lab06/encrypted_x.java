/////////////
///CSE 2 encrypted_x
///Luke Christman
///3-06-2018
///This propgram will:
///prompt the user to enter an integer that will define the length of the 'twist'.
///The program will use while statements and breaks to generate a twist consisting of slashes and X's.
/////////////

import java.util.Scanner; 

public class encrypted_x{
  public static void main(String[] args) {
    int check1 = 0;//declaring a 'counter' that will be used to exit the first while loop when the user completes the command properly
    Scanner myScanner1 = new Scanner( System.in );//declaring first scanner
    System.out.print("Please enter an integer between 0 and 100: ");
    myScanner1.hasNextInt();//checks to see if the value entered was an integer
    int input = 0; //delcaring the user's input outside of the while loop
    while (check1 < 1){
      if ( myScanner1.hasNextInt() == true){//if the entered value if an integer, continue through the loop
        ++check1;//add one to the counter to exit the while loop once completed
        input = myScanner1.nextInt();//assign the value as input
        if (input < 0 || input > 100){//if the input is not in the range of 0-100, continue through while loop
          check1 = 0;//set the counter back to 0 to run the while loop again and reprompt the user to enter a number
          System.out.print("Error: the value entered is not in range. please reenter and integer between 0 and 100: ");//repromt the user to enter a number
        }
      }                                          
      else{
        System.out.print("Error: the value entered is not an integer. please reenter and integer between 0 and 100: ");//prompt the user to enter anothe rnumber if they didn't enter an integer
        myScanner1.next();//assigns next inputted valuie to myScanner1and reruns while loop
      }
    }
    for (int i=0; i < input; i++){//this for loop will be used create the number of rows of the pattern
      for (int j=0; j < input; j++){//creating an inside for loop that will create the pattern 
        if (i == j || i== (input - (j+1))){
          System.out.print(" ");//prints a space when the line number is equal to the character number from the right or left of the sequence
        }
        else{
          System.out.print("*");//fills stars in everywhere else
        }
      }
      System.out.println();//skips to the next line of the pattern and reruns the for loop
    } 
  }
}
                     
                   
                     
    