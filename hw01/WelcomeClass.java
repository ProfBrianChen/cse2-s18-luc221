/////////////
///CSE 2 WelcomeClass
///Luke Christman
///1-29-2018
/////////////
public class WelcomeClass {
  public static void main(String[] args) {
    //Prints a welcome statement to the terminal window that includes my lehigh ID
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-L--U--C--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Luke Christman and I");
    System.out.println("I am a freshman undwergraduate");
    System.out.println("student at Lehigh University. I");
    System.out.println("am from Wilmington, DE, and some");
    System.out.println("of my hobbies include skiing,");
    System.out.println("playing soccer, and swimming.");
    System.out.println("Have a nice day!");
  }
}