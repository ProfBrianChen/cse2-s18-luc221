/////////////
///CSE 2 Hw05
///Luke Christman
///3-06-2018
///This propgram will:
///promt the user to input information about the course they are taking. The program
///will use while loops and scanners in order to ask the user, and then re-ask if the user inputs
///something that doesn't make sense, such as a double when an integer is needed or a 
///number when a word/string is needed.
///I know I wasn't required to print out what the user entered, but I figured this would make it easier to grade.
/////////////

import java.util.Scanner; 

public class Hw05{
  public static void main(String[] args) {
    int counter = 0;//declaring a counter that will be used to progress through different while loops
    Scanner myScanner1 = new Scanner( System.in );//declaring first scanner
    System.out.print("What is the course number?: ");//asking the user what the course number is
    myScanner1.hasNextInt();//varifying that the entered value is an integer
        int CourseNumber = 0;//declaring CourseNumber outside of the while loop
      while (counter < 1) {
        if (myScanner1.hasNextInt() == true){//if the value is an integer, continue through if statement
          CourseNumber = myScanner1.nextInt();//assigning CourseNumber to entered value
          ++counter;//adding one to the counter in order to exit the while loop and continue into the next one
          System.out.println(CourseNumber);
        }
        else{//if the user does not enter an integer, this if statement is carried out
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as an integer: ");//prints an error message and prompts user to reenter the number
          myScanner1.next();//assigns this entered value to myScanner1 and goes through the while loop again
      }
    }
    Scanner myScanner2 = new Scanner( System.in );//declares a second scanner
    System.out.print("What is the name of the department?: ");
    myScanner2.hasNext("[a-zA-Z]+");//checks to see if what was entered by the user consists only of letters, not numbers
    String DepartmentName = "";//declaring DepartmentName outside of the while loop
      while (counter>=1 && counter<2){
        if (myScanner2.hasNext("[a-zA-Z]+") == true){//if the entered value is a string consisting of only letters, continue through if statement
          DepartmentName = myScanner2.nextLine();//assigns DepartmentName to whatever the user types, regardless of how may words. The entire line is assigned.
          System.out.println(DepartmentName);
          ++counter;//adds to the counter to exit the while loop
          
    }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as a word): ");//promtps user to reenter the name if they didn't type a word
          myScanner2.nextLine();//assigns the entered value to myScanner2 and restarts the while loop
        }
  }
      Scanner myScanner3 = new Scanner( System.in );//declares a third scanner
    System.out.print("How many times does the class meet in a week?: ");
    myScanner3.hasNextInt();//checks to make sure the user entered an integer
    int NumberofMeetings = 0;//declares NumberofMeetings outside the while loop
      while (counter>=2 && counter<3){
        if (myScanner3.hasNextInt() == true){//if the value is an integer, continue through if statement
          NumberofMeetings = myScanner3.nextInt();//assigns NumberofMeetings to the entered value
          System.out.println(NumberofMeetings);
          ++counter;//adds one to the counter to exit the while loop
    }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as an integer: ");//promtps the user to reenter a value as an integer
          myScanner3.next();//assigns this value to myScanner3 and reruns the while loop
        }
  }
       Scanner myScanner4 = new Scanner( System.in );//declares a fourth scanner
    System.out.print("What time does the class start?: ");
    myScanner4.hasNext("[0-9]+:[0-9]+");//checks if the user has entered a valid time (must contain numbers before and after a colon)
    String StartTime = "";//declares StartTime outside of the while loop
      while (counter>=3 && counter<4){
        if (myScanner4.hasNext("[0-9]+:[0-9]+") == true){//if the user enters a valid time, continue through the if statement
          StartTime = myScanner4.nextLine();//assign the entered value to StartTime
          System.out.println(StartTime);
          ++counter;//add one to the counter in order to exits the while loop and continue to the next
    }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as a time (11:30 for example): ");//prompts the user to enter a valid time
          myScanner4.nextLine();//assigns the new entered value to myScanner4 and reruns the while loop with this value
        }
  }
         Scanner myScanner5 = new Scanner( System.in );//declares a fifth scanner
    System.out.print("What is the instructor's name? ");
    myScanner5.hasNext("[a-zA-Z]+");//checks to make sure the user input only letters
    String InstructorName = "";//declares InstructorNameoutside the while loop
      while (counter>=4 && counter<5){
        if (myScanner5.hasNext("[a-zA-Z]+") == true){//if the user input only letters, continue through if statement
          InstructorName = myScanner5.nextLine();//assigns the entered value to InstructorName (assings the entire line, reguardless of how many words)
          System.out.println(InstructorName);
          ++counter;//add one to the counter to continue to the next while loop
    }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as a word: ");//prompts user to enter an actual word
          myScanner5.nextLine();//assigns this new entered value to myScanner5 and reruns the while loop
        }
  }
             Scanner myScanner6 = new Scanner( System.in );//declares a sixth scanner
    System.out.print("How many students are in the course?: ");
    myScanner6.hasNextInt();//checks to see if the entered value is an integer
    int numberOfStudents = 0;//declares numberOfStudents outside the while loop
      while (counter>=5 && counter<6){
        if (myScanner6.hasNextInt() == true){//if it is an integer, continue through if statement
          numberOfStudents = myScanner6.nextInt();//assign the entered value to numberOfStudents
          System.out.println(numberOfStudents);
          ++counter;//add one to the counter to exit the while loop 
    }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as an integer: ");//prompts the user to reenter an integer if they did not originally
          myScanner6.next();//assigns this value to myScanner6 and reruns the while loop.
        }
  }
 }
}