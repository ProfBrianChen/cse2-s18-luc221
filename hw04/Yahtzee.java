/////////////
///CSE 2 Yahtzee
///Luke Christman
///2-20-2018
///This propgram will:
///Simulate a game of Yahtzee by generating five random numbers that would appear on dice,
///then scoring the user based on the roll. The user also has the option to manually choose what he or she rolls.
///This can all be achieved using scanners, randomly generated numbers, if statements, modulus, and switches.
/////////////

import java.util.Scanner;

public class Yahtzee{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    //Intitiating the 5 die rolls outside of the if statement. The user is rolling 5 dice.
    int DieRoll1 = 0;
    int DieRoll2 = 0;
    int DieRoll3 = 0;
    int DieRoll4 = 0;
    int DieRoll5 = 0;
    System.out.print("Would you like a Random Roll or would you like to pick your numbers? Type '1' for random and '2' for choice: ");//asks user if they would like a random roll or choose a specific roll
    int GameStyle = (int)myScanner.nextDouble();//Designates the users input as GameStyle
    
    if ( GameStyle == 1 ){
      DieRoll1 = (int)(Math.random()*6+1);//Generates a random number between 1 and 6 for die 1
      DieRoll2 = (int)(Math.random()*6+1);//Generates a random number between 1 and 6 for die 2
      DieRoll3 = (int)(Math.random()*6+1);//Generates a random number between 1 and 6 for die 3
      DieRoll4 = (int)(Math.random()*6+1);//Generates a random number between 1 and 6 for die 4
      DieRoll5 = (int)(Math.random()*6+1);//Generates a random number between 1 and 6 for die 5
    }
    if ( GameStyle == 2 ){
      System.out.print("Enter your roll as a 5 digit number, each of the 5 digits representing one die. There must be 5 numbers entered, each being a number within the interval 1-6: ");//if the user selects GameStyle 2, the can type a 5 digit number, each integer representing the value of the 5 dice
      int MyRollChoice = (int)myScanner.nextDouble();//Assigns this 5 digit number at MyRollChoice
      if ( MyRollChoice >= 100000 ){
        System.out.println("Error: unrecognized roll choice - too many digits");//if the user types more than 5 numbers, an error message will pop up
        return;
      }
      //In order to isolate each of the five integers, the 5 digit number was divided by the correct power of 10, explicitely
      //casted as an integer, and then taken a modulus 10 in order to isolate each value and assign each to a die.
      DieRoll1 = (int)(MyRollChoice/10000)%10;
      DieRoll2 = (int)(MyRollChoice/1000)%10;
      DieRoll3 = (int)(MyRollChoice/100)%10;
      DieRoll4 = (int)(MyRollChoice/10)%10;
      DieRoll5 = MyRollChoice%10;
      if ( DieRoll1 <=0 || DieRoll2 <=0 || DieRoll3 <=0 || DieRoll4 <=0 || DieRoll5 <=0 ){
        System.out.println("Error: unrecognized roll choice - one or more digits are not in the required interval of 1-6");
        return;
      }
      if ( DieRoll1 >=7 || DieRoll2 >=7 || DieRoll3 >=7 || DieRoll4 >=7 || DieRoll5 >=7 ){
        System.out.println("Error: unrecognized roll choice - one or more digits are not in the required interval of 1-6");
        return;
      }
      //The above code forces the user to input numbers between 1 and 6. If the value is too hgih or low, an error will pop up and the user will be autmatically exited.
    }
    //UPPER SECTION
    //Declaring these variables outside of the if statement. They will be used to count how many of each number are rolled.
    int Aces = 0;
    int Twos = 0;
    int Threes = 0;
    int Fours = 0;
    int Fives = 0;
    int Sixes = 0;
    
    switch (DieRoll1){
      case 1:
        Aces++;
          break;
      case 2:
        Twos++;
          break; 
      case 3:         //if The first Die is a 1, 1 is added to the number of aces. if the first die is a 2,                      
        Threes++;     // 1 is added to the number of twos, and the same procedure go for threes fours and fives.                        
          break;      //This process will be repeated for the other 4 dice.
      case 4:
        Fours++;
          break; 
      case 5:
        Fives++;
          break; 
      case 6:
        Sixes++;
          break;  
    }
       switch (DieRoll2){
      case 1:
        Aces++;
          break;
      case 2:
        Twos++;
          break; 
      case 3:
        Threes++;
          break; 
      case 4:
        Fours++;
          break; 
      case 5:
        Fives++;
          break; 
      case 6:
        Sixes++;
          break;  
    }
       switch (DieRoll3){
      case 1:
        Aces++;
          break;
      case 2:
        Twos++;
          break; 
      case 3:
        Threes++;
          break; 
      case 4:
        Fours++;
          break; 
      case 5:
        Fives++;
          break; 
      case 6:
        Sixes++;
          break;  
    }
       switch (DieRoll4){
      case 1:
        Aces++;
          break;
      case 2:
        Twos++;
          break; 
      case 3:
        Threes++;
          break; 
      case 4:
        Fours++;
          break; 
      case 5:
        Fives++;
          break; 
      case 6:
        Sixes++;
          break;  
    }
       switch (DieRoll5){
      case 1:
        Aces++;
          break;
      case 2:
        Twos++;
          break; 
      case 3:
        Threes++;
          break; 
      case 4:
        Fours++;
          break; 
      case 5:
        Fives++;
          break; 
      case 6:
        Sixes++;
          break;  
    }

    int UpperSectionTotal = Aces + Twos*2 + Threes*3 + Fours*4 + Fives*5 + Sixes*6;//Totals the upper sections score by adding up the total number of each die multiplied by 1 2 3 4 5, or 6, respectively.
    int UpperSectionTotalWithBonus = 0;//initiates variable outside if statement
    if ( UpperSectionTotal >=63 ) {
      UpperSectionTotalWithBonus = UpperSectionTotal+35;//if the uppsersection total is greater than 63, a bonus of 35 points will be added to the upper section score
    }
    else {
      UpperSectionTotalWithBonus = UpperSectionTotal+0;//if it is below 63, nothing happens
    }
    System.out.println("The Upper Section Total is " + UpperSectionTotal);//prints the upper section score
    System.out.println("The Upper Section Total with bonus is " + UpperSectionTotalWithBonus);//prints the upper section score, including bonus
    
    //LOWER SECTION
    int Yahtzee = 0;
    if ( DieRoll1 == DieRoll2 && DieRoll2 == DieRoll3 && DieRoll3 == DieRoll4 && DieRoll4 == DieRoll5){
      Yahtzee = 50;//if all five dice are the same, the user gets 50 points
    }
    int ThreeOfAKind = 0;
    int FullHouse = 0;
    if ( Aces == 3 || Twos == 3 || Threes == 3 || Fours == 3 || Fives == 3 || Sixes ==3 ){
      ThreeOfAKind = UpperSectionTotal;//if 3 dice are the same, the user gets the sum of all three dice plus the sum of the other two
      if( Aces == 2 || Twos == 2 || Threes == 2 || Fours == 2 || Fives == 2 || Sixes ==2 ){
        FullHouse = 25;//if the user rolls both a triple AND a pair of two, the user gets 25 points for a full house
      }
    }
    int FourOfAKind = 0;
    if ( Aces == 4 || Twos == 4 || Threes == 4 || Fours == 4 || Fives == 4 || Sixes ==4 ){
      FourOfAKind = UpperSectionTotal;//if the user rolls four of the same number, these 4 dice are added up and the other dice is also summed
      ThreeOfAKind = 0;
    }
    int SmallStraight = 0;
    if ( (Aces >= 1 && Twos >= 1 && Threes >= 1 && Fours >= 1) || (Twos >= 1 && Threes >= 1 && Fours >= 1 && Fives >=1) || (Threes >= 1 && Fours >=1 && Fives >= 1)){
      SmallStraight = 30;//if the user rolls a sequence of four in a row, they get 30 points
    }
    int LargeStraight = 0;
    if ( (Aces >= 1 && Twos >= 1 && Threes >= 1 && Fours >= 1 && Fives >=1) || (Twos >= 1 && Threes >= 1 && Fours >= 1 && Fives >=1 && Sixes >=1)){
      LargeStraight = 40;//if the user rolls a sequence of 5 in a row, they get 40 points
      SmallStraight = 0;
    }
    int Chance = 0;
      if ( ThreeOfAKind == 0 && FourOfAKind == 0 && Yahtzee == 0 && SmallStraight == 0 && LargeStraight == 0 && FullHouse ==0 ){
      Chance = Aces + Twos*2 + Threes*3 + Fours*4 + Fives*5 + Sixes*6;//if the user doesn't get any special combinations iek Yahtzee or a three of a kind, all the numbers will be added up.
      }
    
    int LowerSectionTotal = Yahtzee + ThreeOfAKind + FourOfAKind + FullHouse + SmallStraight + LargeStraight + Chance;//finds the total of the lower section by adding up all the potential points
    System.out.println("The Lower Section Total is " + LowerSectionTotal);
    int GrandTotal = LowerSectionTotal + UpperSectionTotalWithBonus;//finds the total score by adding the lower section score to the upper section score, including the bonus
    System.out.println("The Grand Total is " + GrandTotal);
  }
}
