/////////////
///CSE 2 Area
///Luke Christman
///3-27-2018
///This propgram will:
///create methods outside the main method to either calculate the area of the shape, or check if the input the user enters
///is a double and reprompts for a new input. 
/////////////

import java.util.Scanner;

public class Area{    

  public static double rectangleAreaGenerator(double width, double height){//creates a method that returns a double
    return width*height;//finds the area of the rectangle
  }
  public static double triangleAreaGenerator(double base, double height){//creates a method that returns a double
    return .5*base*height;//finds the area of a triangle
  }
  public static double circleAreaGenerator(double radius){//creates a method that returns a double
    return Math.PI*radius*radius;//finds the area of a circle
  }
  public static double inputChecker(){//creates a method that returns a double
    Scanner myScanner1 = new Scanner( System.in );
    boolean val1 = myScanner1.hasNextDouble();//checks if the scanner is a double
    double val2 = 0;//delcares this value outside the while loop which will be returned
    int counter = 0;//delcaring counter outside the while loop
    while (counter < 1){
    if (val1==true){//if the entered number is a double:
      counter++;//add one to the counter to exit the while loop
      val2 = myScanner1.nextDouble();//set val2 equal to the scanner
    }
    if (val1 == false){//if the input is not a double:
      System.out.print("The value entered is not a double. Please reenter a double: ");//repromt the user to enter another value
      myScanner1.next();//discards whatever was in the scanner
      val1 = myScanner1.hasNextDouble();//checks if the entered value is a double again, and restarts the while loop
    }
  }
    return val2;//return the value to the main method
  }
    

  
  
  public static void main(String[] args){
    int counter = 0;//declaring counter outside the while loop
    String shape = ""//declaring the shape outside the while loop
    Scanner myScanner1 = new Scanner( System.in );
    System.out.print("What shape would you like to find the area of - triangle, circle, or rectangle?: ");//prompts the user to enter a shape
    myScanner1.hasNext("triangle");//returns true or false if the user types this
    myScanner1.hasNext("circle");//returns true or false if the user types this
    myScanner1.hasNext("rectangle");//returns true or false if the user types this
    while ( counter < 1){
      if ( myScanner1.hasNext("triangle") == true || myScanner1.hasNext("circle") == true || myScanner1.hasNext("rectangle") == true ){//if the user types in a correct shape
        shape = myScanner1.next();//set the shape equal to the entered input
        ++counter;//add one to the counter and exit the while loop
      }
      else{
        System.out.print("Error: you entered an invalid response. Please type triangle, circle or rectangle: ");//if they dont enter a correct shape
        myScanner1.next();//repromt the user to enter a value and erase the previous entered value
      }
    }
    double trianglebase = 0;//delcaring the inputs for the triangle outside the if statements
    double triangleheight = 0;
    double triangleArea = 0;
    if (shape.equals("triangle") == true){//if the user enters a triangle
      System.out.print("Please enter the base of the triangle: ");
      trianglebase = inputChecker();//run trianglebase through the inputChecker
      System.out.print("Please enter the height of the triangle: ");
      triangleheight = inputChecker();//run triangleheight through the inputChecker
      triangleArea = triangleAreaGenerator(trianglebase, triangleheight);//run triangleArea through triangleAreaGenerator
      System.out.println("The area of the triangle is " + triangleArea);
    }
    double circleArea = 0;
    double circleRadius = 0;
    if(shape.equals("circle")==true){
      System.out.print("Please enter the radius of the circle: ");
      circleRadius = inputChecker();
      circleArea = circleAreaGenerator(circleRadius);
      System.out.println("The area of the circle is " + circleArea);
    }
    double rectangleWidth = 0;
    double rectangleLength = 0;
    double rectangleArea = 0;
    if (shape.equals("rectangle") == true){
      System.out.print("Please enter the length of the rectangle: ");
      rectangleLength = inputChecker();
      System.out.print("Please enter the width of the rectangle: ");
      rectangleWidth = inputChecker();
      rectangleArea = rectangleAreaGenerator(rectangleWidth,rectangleLength);
      System.out.println("The area of the rectangle is " + rectangleArea);
    }

  }
}