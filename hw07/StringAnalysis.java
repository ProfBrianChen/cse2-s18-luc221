/////////////
///CSE 2 StringAnalysis
///Luke Christman
///3-27-2018
///This propgram will:
///use overloaded methods to analyze a string that is inputted by the user. If the string is all letters, the program 
///will print true. If not, the program will print false
/////////////

import java.util.Scanner;

public class StringAnalysis{
  
  public static boolean stringAnalysis(String analysis){//creating a method that returns a boolean 
    int numberCharacters = analysis.length();//designates numberCharacters to be the length of the inputted string
    boolean firstAnalysis = true;//defining the boolean outside of the scope 
    for (int i = 0; i < numberCharacters; i++){//for loop runs until the loop reaches the number characters in the string
      if (analysis.charAt(i) >= 'a' && analysis.charAt(i) <= 'z'){//if the character at the first, second, third... spot is a letter, continue through the if statement
        firstAnalysis = true;//set the boolean equal to true and keep going through the for loop
      }
      else{//if the user enters something that isnt a letter:
        firstAnalysis = false;//set boolean equal to false
        return firstAnalysis;//return this boolean
      }
    }
    return firstAnalysis;//return the boolean of 'true' once every character is evaluated.
  }
  public static boolean stringAnalysis(String analysis, int number){//creating a method that returns a boolean that will run when gicin both a string and integer
    boolean secondAnalysis = true;//declaring boolean outside of the scope
    for (int i = 0; i < number; i++){//for loop runs until it reaches the specified integer that was given
      if (analysis.charAt(i) >= 'a' && analysis.charAt(i) <= 'z'){//if the character is a letter:
        secondAnalysis = true;//set boolean equal to true and keep going through for loop
      }
      else{
        secondAnalysis = false;//set boolean equal to false
        return secondAnalysis;//retrun the boolean as false once it hits a charcter that isn't a letter
      }
    }
    return secondAnalysis;//return true if the entire string is letters
  }
  
  public static void main(String[] args) {
    Scanner myScanner1 = new Scanner( System.in );
    System.out.print("Please enter a string that you would like to be analyzed: ");//prompt user to enter a string
    String aquiredString = myScanner1.next();//set aquiredString equal to the input
    Scanner myScanner2 = new Scanner( System.in );
    Scanner myScanner3 = new Scanner( System.in );
    System.out.print("Would you like to evaluate the entire string or just part of it? Type 1S for the full string and 2 for only part: ");//promt user to pick if they would like to evaluate the whole string or only part of it
    int analysisType = myScanner2.nextInt();//set analysisType equal to the input value
    boolean x = true;//decalare the boolean outside of the foreloop
    if (analysisType == 1){//if the user wants to evaluate the whole string:
      x = stringAnalysis(aquiredString);//run x through the stringAnalysis method. Overloading will choose the correct method
    }
    if (analysisType == 2){//if the user wants to evaluate only part of the string
      System.out.print("Please enter the number of letters you'd like to evaluate in the string: ");//promt user to enter the number of letters in the string they want to evaluate
      
      int stringLength = myScanner3.nextInt();  //set stringLength equal to the input value
      if (stringLength > aquiredString.length()){//if the user enters an integer that is longer than the length of the string:
        stringLength = aquiredString.length();//set the integer equal to the length of the string
      }
      x = stringAnalysis(aquiredString,stringLength);//run x through the stringAnalysis method. Overloading will choose the correct method
      
    }
  System.out.println(x);//print the evaluation of the string: true if the is all letters, false if not.
 }
}