/////////////
///CSE 2 TwistGenerator
///Luke Christman
///3-06-2018
///This propgram will:
///prompt the user to enter an integer that will define the length of the 'twist'.
///The program will use while statements and breaks to generate a twist consisting of slashes and X's.
/////////////

import java.util.Scanner; 

public class TwistGenerator{
  public static void main(String[] args) {
    int check = 0;//declaring a 'counter' that will be used to exit the first while loop when the user completes the command properly
    Scanner myScanner1 = new Scanner( System.in );//declaring first scanner
    System.out.print("Please enter an integer that will define the length of the Twist: ");
    myScanner1.hasNextInt();//checks to see if the value entered was an integer
    int length = 0;//declaring scope outside of the while loop
      while (check < 1) {
        if (myScanner1.hasNextInt() == true){//if the value is an integer, continue through if statement
          length = myScanner1.nextInt();//assigning length to entered value
          ++check;//adding one to the counter in order to exit the while loop and continue into the next one
        }
        else{
          System.out.print("Error: the response you have entered is invalid. Please reenter your response as an integer: ");//prints an error message and prompts user to reenter the number
          myScanner1.next();//assigns to reentered number to myScanner1 and reruns the while loop with the new value
        }
      }
         int counter=0;//initiating counter outside of the while loop
        //PRINTING TOP ROW OF TWIST
        while(counter<length){
            System.out.print("\\");//prints first slash
            counter++;//adds one to the counter
            if(counter==length){
                break;//exits the while loop if counter is equal to the length
            }
            System.out.print(" ");//prints a space
            counter++;//adds one to the same counter
            if(counter==length){
                break;//exits the while loop if the counter equals the length and continues to the next row
            }
            System.out.print("/");
            counter++;
            if(counter==length){
                break;//ifs and breaks after every print statement ensure that the while loop will not finish after after the counter surpasses the length. 
            }
         
        }
       System.out.println();//empty print to skip to the next line (The second row)
    
    //PRINTING SECOND ROW OF TWIST
       int counter1=0;//declaring a new counter outside of the while loop
       while(counter1<length){
            System.out.print(" ");
            counter1++;
            if(counter1==length){
                break;
            }
            System.out.print("X");
            counter1++;
            if(counter1==length){
                break;
            }
            System.out.print(" ");
            counter1++;
            if(counter1==length){
                break;
            }
         
        }
       System.out.println();
    
    //PRINTING THIRD ROW OF TWIST
       int counter2=0;//declaring a third counter outside the while loop
       while(counter2<length){
            System.out.print("/");
            counter2++;
            if(counter2==length){
                break;
            }
            System.out.print(" ");
            counter2++;
            if(counter2==length){
                break;
            }
            System.out.print("\\");
            counter2++;
            if(counter2==length){
                break;
            }
        }
        System.out.println();
    
  }
}