/////////////
///CSE 2 DrawPoker
///Luke Christman
///4-16-2018
///This propgram will:
///Simulate a game of Draw Poker with hands of size 7. The program will shuffle a deck of cards, deal the 
///cards to the users, and then calculate the winner based off of how many pairs, triples, full houses,
///and flushes are present in each hand.
/////////////

import java.util.Random; 
public class DrawPoker 
{
	public static int[] shuffledDeckGenerator(int[] shuffledDeck){
		Random rand = new Random(); //creates random funcation as rand
		for (int i = 0; i < 52; i++){ //for every card in a standard deck of cards
			int target = (int) (rand.nextInt(52)); //creates a target number from 1-52
			int mix = shuffledDeck[target]; //creates int mix that shuffles the deck
			shuffledDeck[target] = shuffledDeck[i];
			shuffledDeck[i] = mix; //this creates the shuffled deck as the temp.
		}
		return shuffledDeck;//return the shuffled deck
	}

	public static String cardGenerator(int card){
		String cardIdentity = ""; //initalizes the identity of the card
		String cardSuit = ""; //initializes the suit
		int identity = card%13; //modulus determines identity
		int suit = card/13; //division determines suit
		switch( identity ){ //evaluates idenity using a switch statent, assigning the idenities to the remainders of suit
	      case 0: 
					cardIdentity = "Ace";
	      	break;
	      case 1: 
					cardIdentity = "2";
	        break;
	      case 2: 
					cardIdentity = "3";
	        break;
	      case 3: 
					cardIdentity = "4";
	        break;
	      case 4:
					cardIdentity = "5";
	        break;
	      case 5: 
					cardIdentity = "6";
	        break;
	      case 6:
					cardIdentity = "7";
	        break;
	      case 7: 
					cardIdentity = "8";
	        break;
	      case 8: 
					cardIdentity = "9";
	        break;
	      case 9: 
					cardIdentity = "10";
	        break;
	      case 10: 
					cardIdentity = "Jack";
	        break;
	      case 11: 
					cardIdentity = "Queen";
	        break;
	      case 12: 
					cardIdentity = "King";
					break;
	  } //assigns rank
	    switch( suit ){ //creating a switch statement that assigns suits depending on the division 
	      case 0: 
					cardSuit = "Clubs";
	        break;
	      case 1: 
					cardSuit = "Hearts";
	        break;
	      case 2: 
					cardSuit = "Spades";
	        break;
	      case 3: 
					cardSuit = "Diamonds";
	        break;
	     } 
	    String nameOfCard = cardIdentity + " of "  + cardSuit; //creates a string for the name of the card
	    return nameOfCard;
	}
	
	public static boolean findPair(int[] playerHand){
		//creating twelve different counters to count the amount of each card present in the dealt hand
		int counter0 = 0;
		int counter1 = 0;
		int counter2 = 0;
		int counter3 = 0;
		int counter4 = 0;
		int counter5 = 0;
		int counter6 = 0;
		int counter7 = 0;
		int counter8 = 0;
		int counter9 = 0;
		int counter10 = 0;
		int counter11 = 0;
		int counter12 = 0;		
		//The homework assignment did not specify how many cards, so I chose a hand of 7 cards, similar to that of Texas Holdem
		for (int i = 0; i < 7; i++){
		
		switch(playerHand[i]%13){
		case 0: 
			counter0++; 
			break;
		case 1: 
			counter1++; 
			break;
		case 2: 
			counter2++; 
			break;
		case 3: 
			counter3++; 
			break;
		case 4: 
			counter4++; 
			break;
		case 5: 
			counter5++; 
			break;
		case 6: 
			counter6++; 
			break;
		case 7: 
			counter7++; 
			break;
		case 8: 
			counter8++; 
			break;
		case 9: 
			counter9++; 
			break;
		case 10: 
			counter10++; 
			break;
		case 11: 
			counter11++; 
			break;
		case 12:
			counter12++; 
			break;
		}
		}
		//if there are two or more of a single card, the method will return true to show that there is a double present
		if( (counter0 >= 2) || (counter1 >= 2) || (counter3 >= 2) || (counter4 >= 2) || (counter5 >= 2) || (counter6 >= 2) || (counter7 >= 2) || (counter8 >= 2) || (counter9 >= 2) || (counter10 >= 2) || (counter11 >= 2) || (counter12 >= 2) ){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	public static boolean findTriple(int[] playerHand){
		//refer to above method for explanation of this method. Essentially, it is the same method except counters must reach three to return
		//a double, showing that there is a triple present
		int counter0 = 0;
		int counter1 = 0;
		int counter2 = 0;
		int counter3 = 0;
		int counter4 = 0;
		int counter5 = 0;
		int counter6 = 0;
		int counter7 = 0;
		int counter8 = 0;
		int counter9 = 0;
		int counter10 = 0;
		int counter11 = 0;
		int counter12 = 0;
		
		for (int x = 0; x < 7; x++){
		
		switch(playerHand[x]%13){
		case 0: 
			counter0++; 
			break;
		case 1:
			counter1++; 
			break;
		case 2: 
			counter2++; 
			break;
		case 3: 
			counter3++; 
			break;
		case 4: 
			counter4++; 
			break;
		case 5: 
			counter5++; 
			break;
		case 6: 
			counter6++; 
			break;
		case 7:
			counter7++; 
			break;
		case 8: 
			counter8++; 
			break;
		case 9:
			counter9++; 
			break;
		case 10:
			counter10++; 
			break;
		case 11: 
			counter11++; 
			break;
		case 12: 
			counter12++; 
			break;
		}
		}
		if( (counter0 >= 3) || (counter1 >= 3) || (counter3 >= 3) || (counter4 >= 3) || (counter5 >= 3) || (counter6 >= 3) || (counter7 >= 3) || (counter8 >= 3) || (counter9 >= 3) || (counter10 >= 3) || (counter11 >= 3) || (counter12 >= 3) ){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static boolean findFullHouse(int[] playerHand){
		
		findPair(playerHand);//
		findTriple(playerHand);
		//if there is both a triple and double present, then the method will return true to show that there is a full house present
		if( findPair(playerHand) == true && findTriple(playerHand) == true ){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static boolean findFlush(int[] playerHand){
		//method will evaluate the hand to see if there are 5 of the same suit. delcaring counters that will be used to count each suit
		int counter0 = 0;
		int counter1 = 0;
		int counter2 = 0;
		int counter3 = 0;
		//for loop to check each member of the array of the players hand
		for (int x = 0; x < 7; x++){
		
		switch(playerHand[x]/13) //checking suiot by diving by 13. switch statement is similar to that in the cardGenerator method
		{
		case 0: counter0++; 
			break;
		case 1: counter1++; 
			break;
		case 2: counter2++; 
			break;
		case 3: counter3++; 
			break;
		}
	}
		if(((counter0 >= 5) || (counter1 >= 5)) || ((counter2 >= 5) || (counter3 >= 5))){
			return true;
		}
		else{
			return false;
		}
		//when true this shows that there is a flush present
	}
		
	public static void main (String args[]){//creates the deck of cards
		int[] deck; //declaring an array of integers
		deck = new int[52];//allocating its length to 52, the number of cards in a deck
		for(int i = 0; i < 52; i++){
			deck[i] = i;//create an array of 52 members each holding a different value
		}
		
		shuffledDeckGenerator(deck);//passes the array into the shuffledDeckGenerator to shuffle the cards 
		//delcaring arrays of length 7 for each player
		int[] player1Cards;
		int[] player2Cards;
		player1Cards = new int[7];
		player2Cards = new int[7];
		//dealing the cards one by one. The first cards to player one, the second to player 2, the third to player 1, etc. 		
		player1Cards[0] = deck[0];
		player2Cards[0] = deck[1];
		player1Cards[1] = deck[2];
		player2Cards[1] = deck[3];
		player1Cards[2] = deck[4];
		player2Cards[2] = deck[5];
		player1Cards[3] = deck[6];
		player2Cards[3] = deck[7];
		player1Cards[4] = deck[8];
		player2Cards[4] = deck[9];
		player1Cards[5] = deck[10];
		player2Cards[5] = deck[11];
		player1Cards[6] = deck[12];
		player2Cards[6] = deck[13];
		
		//running each players hand through the methods to check for pairs, triples, full houses, and flushes
		findPair(player1Cards);
		findPair(player2Cards);
		findTriple(player1Cards);
		findTriple(player2Cards);
		findFullHouse(player1Cards);
		findFullHouse(player2Cards);
		findFlush(player1Cards);
		findFlush(player2Cards);
		//iniating the scores for each player that will be changed based on what the hand is
		int player1Score = 0;
		int player2Score = 0;	
		//delcaring counters to count how many pairs are present in each hand
		int pairCounter1 = 0;
		int pairCounter2 = 0;
		
		if(findPair(player1Cards) == true){//if there is a pair, add one to both the score and counter
						player1Score++;
						pairCounter1++;
		}
		if(findPair(player2Cards) == true){
						player2Score++;
						pairCounter2++;
		}		
		//initiating counters to count the number of triples in the hand
		int tripleCounter1 = 0;
		int tripleCounter2 = 0;

		if(findTriple(player1Cards) == true){//if there is a triple present add one to the counter and add 2 to the score (triples are worth more than doubles)
			player1Score = player1Score+2;
			tripleCounter1++;
		}
		if(findTriple(player2Cards) == true){
			player2Score = player2Score+2;
			tripleCounter2++;
			}		
		//initiating counters to count the number of fullhouses
		int fullHouseCounter1 = 0;
		int fullHouseCounter2 = 0;
		if (findFullHouse(player1Cards) || findFullHouse(player2Cards))
		{
			if(findFullHouse(player1Cards) == true){
				player1Score = player1Score+10;//adding 10 to the score in the presence of a full house because these are worth the most
				fullHouseCounter1++;
			}
			if(findFullHouse(player2Cards) == true){
				player2Score = player2Score+10;
				fullHouseCounter2++;
			}
		}
		
		//declaring counters to count flushes
		int flushCounter1 = 0;
		int flushCounter2 = 0;
		
		if (findFlush(player1Cards) || findFlush(player2Cards)){
			if(findFlush(player1Cards) == true){
				player1Score = player1Score+5;//adding 5 to the score because it is worth more than 2 pairs or 2 tripples but less than full house
				flushCounter1++;
			}
			if(findFlush(player2Cards) == true){
				player2Score = player2Score+5;
				flushCounter2++;
			}
		}	
		//declaring an array of strings of length 14 
		String[] realCards = new String[14];
		
		for( int i = 0; i < 14; i++){
			realCards[i] = cardGenerator(deck[i]);//for each card, generate a random cards 
		}
		//prints out all the cards that each player was dealt
		System.out.println("Player 1 draws cards: " +realCards[0]+", "+realCards[2]+", "+realCards[4]+", "+realCards[6]+", "+realCards[8]+", "+realCards[10]+", "+realCards[12]);
		System.out.println("Player 2 draws cards: " +realCards[1]+", "+realCards[3]+", "+realCards[5]+", "+realCards[7]+", "+realCards[9]+", "+realCards[11]+", "+realCards[13]);
		//prints out all the combinations that each player has
		System.out.println("Player 1's hand: ");
		System.out.println("Pairs: " + pairCounter1 + " \\ Three-of-a-Kind: " +tripleCounter1 + " \\ Flushes: " +flushCounter1 + " \\ Full Houses: " +fullHouseCounter1);
		
		System.out.println("Player 2's hand: ");
		System.out.println("Pairs: " + pairCounter2 + " \\ Three-of-a-Kind: " +tripleCounter2 + " \\ Flushes: " +flushCounter2 + " \\ Full Houses: " +fullHouseCounter2);
		//depending on who has the greater score, print out the winner
		if(player1Score > player2Score){
			System.out.println("Player 1 wins.");
		}
		
		if(player1Score < player2Score){
			System.out.println("Player 2 wins.");
		}
		
		if(player1Score == player2Score){
			System.out.println("Player 1 and 2 tied.");//if scores are the same, print that the players tied
		}
				
	}
}