/////////////
///CSE 2 Arithmetic
///Luke Christman
///2-4-2018
///This propgram will:
///Calculate the cost of buying shirt, belts, and pants
///calculate tax
///apply tax to total purchase
///Find a final trransaction cost
///Make sure all the values have only two decimal places, in order to simulate actual money
/////////////

public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
 // our input data: 
 int numPants = 3; //gives the number of pants being bought
 double pantsPrice = 34.98; //gives the price of one pair ff paints
 int numShirts = 2; //gives the number of shirts being bought
 double shirtPrice = 24.99; //gives the price of one shirt
 int numBelts = 1; //gives the number of belts being bought
 double beltCost = 33.99; //gives the price of one belt 
 double paSalesTax = 0.06; //gives the sales tax in PA, which will be used to calculate the actual cost at checkout
      
      //our calculations
      double totalcostpants = numPants*pantsPrice; //gives the total cost of buying 3 pairs of pants without tax
      double totalcostshirts = numShirts*shirtPrice; //gives the total cost of buying 2 shirts without tax
      double totalcostbelts = numBelts*beltCost; //gives the total cost of buying 1 belt without tax
      
      double salestaxpants = ((int)(numPants*pantsPrice*paSalesTax*100))/100.0; //gives the sales tax applied when buying 3 pairs of pants. I then multiplied this number by 100, converted to an int, and divided by 100.0 in order to create two decimal places.
      double salestaxshirts = ((int)(numShirts*shirtPrice*paSalesTax*100))/100.0; //gives the sales tax applied when buying 2 shirts. I then multiplied this number by 100, converted to an int, and divided by 100.0 in order to create two decimal places.
      double salestaxbelts = ((int)(numBelts*beltCost*paSalesTax*100))/100.0; //gives the sales tax applied when buying 1 belt. I then multiplied this number by 100, converted to an int, and divided by 100.0 in order to create two decimal places.
        
      double totalcostwithouttax = totalcostpants + totalcostshirts + totalcostbelts; //gives the total cost of buying all of the items (shirts belts and pants) before taxes are applied
      
      double totalsalestax = ((int)((salestaxbelts+salestaxshirts+salestaxpants)*100))/100.0; //gives the total sales tax when buying
      
      double grandtotal = totalcostwithouttax+totalsalestax; //gives the total price at checkout of buying all the items. This combines the total tax amount to the total price without tax
      
      //Printing the desired output statements
      System.out.println("3 pairs of pants will cost $"+(totalcostpants)+" without tax. The total applied sales tax to this purchase is $"+(salestaxpants));
      System.out.println("2 shirts will cost $"+(totalcostshirts)+" without tax. The total applied sales tax to this purchase is $"+(salestaxshirts));
      System.out.println("1 belt will cost $"+(totalcostbelts)+" without tax. The total applied sales tax to this purchase is $"+(salestaxbelts));
      System.out.println("The total cost of the shirts, pants, and belts, before tax, will be $"+(totalcostwithouttax));
      System.out.println("The total sales tax applied when buying all of the items will be $"+(totalsalestax));
      System.out.println("Finally, the total cost of buying all the items after tax is applied will be $"+(grandtotal));
   
    }
}