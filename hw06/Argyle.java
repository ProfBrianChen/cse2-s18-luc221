/////////////
///CSE 2 Argyle
///Luke Christman
///3-20-2018
///This propgram will:
///Use scanners to promt the user to enter the dimensions and characters of the argyle pattern, and then use 
///nested for loops to create this pattern.
/////////////

import java.util.Scanner; 

public class Argyle{
  public static void main(String[] args) {
    
    Scanner myScanner1 = new Scanner( System.in );//declaring first scanner
    System.out.print("Please enter a positive integer for the width of the viewing window: ");
    myScanner1.hasNextInt();//checks to see if the value entered was an integer
    int check1 = 0;//declaring a 'counter' that will be used to exit the first while loop when the user completes the command properly
    int viewWidth = 0; //delcaring the view width outside the while loop
    while (check1 < 1){
      if ( myScanner1.hasNextInt() == true){//if the entered value is an integer, continue through the loop
        ++check1;//add one to the counter to exit the while loop once completed
        viewWidth = myScanner1.nextInt();//assign the value as input
        if (viewWidth < 0){//if the input is not a positive, continue through if statement
          check1 = 0;//set the counter back to 0 to run the while loop again and reprompt the user to enter a number
          System.out.print("Error: the value entered is not in range. please reenter an integer that is positive: ");//repromt the user to enter a number
        }
      }                                          
      else{
        System.out.print("Error: the value entered is not an integer. please reenter an integer: ");//prompt the user to enter anothe number if they didn't enter an integer
        myScanner1.next();//assigns next inputted valuie to myScanner1 and reruns while loop
      }
    }
    
    Scanner myScanner2 = new Scanner( System.in );//declaring second scanner
    System.out.print("Please enter a positive integer for the height of the viewing window: ");
    myScanner2.hasNextInt();
    int check2 = 0;
    int viewHeight = 0;
    while (check2 < 1){
      if ( myScanner2.hasNextInt() == true){//if the entered value is an integer, continue through the loop
        ++check2;//add one to the counter to exit the while loop once completed
        viewHeight = myScanner2.nextInt();//assign the value as input
        if (viewHeight < 0){//if the input is not positve, continue through while loop
          check2 = 0;//set the counter back to 0 to run the while loop again and reprompt the user to enter a number
          System.out.print("Error: the value entered is not in range. please reenter an integer that is positive: ");//repromt the user to enter a number
        }
      }                                          
      else{
        System.out.print("Error: the value entered is not an integer. please reenter an integer: ");//prompt the user to enter anothe rnumber if they didn't enter an integer
        myScanner2.next();//assigns next inputted valuie to myScanner1and reruns while loop
      }
    }
    
    Scanner myScanner3 = new Scanner( System.in );//declaring third scanner
    System.out.print("Please enter a positive integer for the width of the Argyle Diamonds: ");
    myScanner3.hasNextInt();
    int check3 = 0;
    int argyleDiamondWidth = 0;
    while (check3 < 1){
      if ( myScanner3.hasNextInt() == true){//if the entered value is an integer, continue through the loop
        ++check3;//add one to the counter to exit the while loop once completed
        argyleDiamondWidth = myScanner3.nextInt();//assign the value as input
        if (argyleDiamondWidth < 0){//if the input is not in the range of 0-100, continue through while loop
          check3 = 0;//set the counter back to 0 to run the while loop again and reprompt the user to enter a number
          System.out.print("Error: the value entered is not in range. please reenter an integer that is positive: ");//repromt the user to enter a number
        }
      }                                          
      else{
        System.out.print("Error: the value entered is not an integer. please reenter an integer: ");//prompt the user to enter anothe rnumber if they didn't enter an integer
        myScanner3.next();//assigns next inputted valuie to myScanner1and reruns while loop
      }
    }
    
    Scanner myScanner4 = new Scanner( System.in );//declaring fourth scanner
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: ");
    myScanner4.hasNextInt();
    int check4 = 0;
    int argyleCenterStripWidth = 0;
    while (check4 < 1){
      if ( myScanner4.hasNextInt() == true){//if the entered value if an integer, continue through the loop
        ++check4;//add one to the counter to exit the while loop once completed
        argyleCenterStripWidth = myScanner4.nextInt();//assign the value as input
        if (argyleCenterStripWidth < 0 || argyleCenterStripWidth%2 == 0 || argyleCenterStripWidth > argyleDiamondWidth/2){//if the input is not positive, is even, or is greater than half the width of the diamond, continue
          check4 = 0;//set the counter back to 0 to run the while loop again and reprompt the user to enter a number
          System.out.print("Error: the value entered is not in range. please reenter and integer that is positive, odd, and less than half the argyleDiamondWidth: ");//repromt the user to enter a number
        }
      }
      else{
        System.out.print("Error: the value entered is not an integer. please reenter an integer: ");//prompt the user to enter another rnumber if they didn't enter an integer
        myScanner4.next();//assigns next inputted valuie to myScanner1and reruns while loop
      }
    }
   
    Scanner myScanner5 = new Scanner( System.in );//declaring fifth scanner
    System.out.print("Enter a character for the first pattern fill: ");
    myScanner5.hasNextInt();
    String temp1 = myScanner5.next();
		char charPattern1 = temp1.charAt(0);//gets the character in the string at int 0
    
    Scanner myScanner6 = new Scanner( System.in );//declaring sixth scanner
    System.out.print("Enter a character for the diamond fill: ");
    myScanner6.hasNextInt();
    String temp2 = myScanner6.next();
		char diamondPattern = temp2.charAt(0);//gets the character in the string at int 0
    
    Scanner myScanner7 = new Scanner( System.in );//declaring seventh scanner
    System.out.print("Enter a character for the stripe pattern fill: ");
    myScanner7.hasNextInt();
    String temp3 = myScanner7.next();
		char stripePattern = temp3.charAt(0);//gets the character in the string at int 0
    
    int totalDiamondsRighttoLeft = viewWidth/(2* argyleDiamondWidth);//Divides the view width by the entire width of the diamond (argyleDiamondWidth is only half the width)
    int totalDiamondsUptoDown = viewHeight/(2*argyleDiamondWidth);//divides the view height by the enitre height of thre diamond which is also twice the argyleDiamondWidth
 
    for (int i=0; i < totalDiamondsRighttoLeft; i++){//this for loop will be used to create the number of columns
      for(int j=0; j < 2*argyleDiamondWidth; j++){//this for loop will be used to create the patterns within the columns
        for( int k=0; k < totalDiamondsUptoDown; k++){//this for loop will be used to create the number of rows
          for( int z=0; z < 2*argyleDiamondWidth; z++){//this for loop will be used to create ther patterns within the rows 
            if (j == z){
              System.out.print(stripePattern);//prints the stripe pattern when the row is the same as the column
            }
            else if ((argyleCenterStripWidth > 1) && (j - (argyleCenterStripWidth - 2) == z)){
              System.out.print(stripePattern);//if the stripe patten is bigger than one and 
            }    
            else if ((argyleCenterStripWidth > 1) && (j == z - (argyleCenterStripWidth - 2))){
              System.out.print(stripePattern);
            }
            else if ((j + z) == (2*argyleDiamondWidth - (argyleCenterStripWidth -2))){
              System.out.print(stripePattern);
            }
            else if ((argyleCenterStripWidth>1) && ((j+z)==(2*argyleDiamondWidth - (argyleCenterStripWidth-3)))){
              System.out.print(stripePattern);
            }
            else if ((argyleCenterStripWidth>1) && ((j+z)==(2*argyleDiamondWidth - (argyleCenterStripWidth-1)))){
              System.out.print(stripePattern);
            }
            else if ((((j+z)>=(argyleDiamondWidth - 1))&&((j+z)<(3*argyleDiamondWidth)))&&(((z-j)<=argyleDiamondWidth))&&((z-j)>=(-1*argyleDiamondWidth))){
              System.out.print(diamondPattern);
            }
            else{
              System.out.print(charPattern1);
            }
          }
        }    
       }
      }
	   }
    } 
//Note to Grader: obviously the code is not finished. I tried different combinations of for loops and felt I was getting
//closer to the final result, but I don't want it to be another day late. 
   
    

      