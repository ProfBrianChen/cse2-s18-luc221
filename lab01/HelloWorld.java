public class HelloWorld{
	
	public static void printRowMajor(int[][] array){
		for (int i = 0; i<array.length; i++){
			for(int j = 0; j<array[i].length; j++){
			System.out.printf(" "+array[i][j]+" ");
			}
			System.out.println();
		}
		
	}
	
	public static void printColMajor(int[][] array){
	for (int i = 0; i<array[i].length; i++){
			for(int j = 0; j<array.length; j++){
				System.out.printf(" "+array[j][i]+" ");
			}
			System.out.println();
		}
		
	}
	
	
  public static void main(String[] args) {
		
		int[][] test = {
			{1,2,3,4,5},
			{6,7,8,9,10},
			{11,12,13,14,15}
		};
		printRowMajor(test);
		
		int [][] testColMajor = {
			{1,6,11},
			{2,7,12},
			{3,8,13},
			{4,9,14},
			{5,10,15}
		};
		printColMajor(testColMajor);
	}
	
	int[][][] array3d = {
		
		{
			{1,2,3},
			{1,2,3},
			{1,2,3}			
		};
		
		{
			{4,5,6},
			{4,5,6},
			{4,5,6}			
		};
		
	}
}

    
    
        
      
      
    

  
  
   

     

        
        
    