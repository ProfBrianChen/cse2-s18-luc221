/////////////
///CSE 2 Convert
///Luke Christman
///2-10-2018
///This propgram will:
///Prompt the user to input the number of affected acres and number of inches 
///of rainfall in order to print out the cubic miles of rainfall using these variables
/////////////

import java.util.Scanner;

public class Convert{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );//tells Scanner that you are creating an instance that will take input from STDIN
    System.out.print("Enter the affected area in acres: ");//prompts the user to type in the original cost of the check
double numAcres = myScanner.nextDouble();//assigns the typed value as a variable numAcres
    System.out.print("Enter the rainfall in the affected area in inches: ");//prompts the user to type in the original cost of the check
double inchesofrain = myScanner.nextDouble();//assigns the typed value as a variable inchesofrain
   double GallonsofRain = (inchesofrain*numAcres)*27154.2876;//converts the number of inches and acres of rain to the number of gallons of rain
   double CubicMilesofRain = GallonsofRain/1.10111714743e12;//converts the gallons of rain to cubic miles of rain 
    System.out.println("There is a rainfall of " + (CubicMilesofRain) + " cubic miles");//prints the number of cubic miles of rainfall
}
}