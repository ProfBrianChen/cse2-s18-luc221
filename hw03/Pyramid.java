/////////////
///CSE 2 Pyramid
///Luke Christman
///2-10-2018
///This propgram will:
///Prompt the user to input the dimensions of a pyramid
///and will then print out the volume of the pyramid given those dimensions
/////////////

import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );//tells Scanner that you are creating an instance that will take input from STDIN
    System.out.print("The square side of the pyramid is (input length): ");//prompts the user to input the side lenght of the pyramid
      double sideLength = myScanner.nextDouble();//assigns the above chosen value as sideLength
    System.out.print("The height of the pyramid is (input height): ");//prompts the user to input the height of the pyramid
      double height = myScanner.nextDouble();//assigns the above chosen value as height
    double pyramidVolume = sideLength*sideLength*height/3;//gives the volume of the pyramid using the equation V=1/3*area of base*height
    System.out.println("The volume inside the pyramid is: "+(pyramidVolume));//Prints the volume of the pyramid
  }
}