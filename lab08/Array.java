/////////////
///CSE 2 Array
/////////////

import java.util.Scanner;

public class Array{
   //main method required for all Java programs
    public static void main(String arg[]){
      int numStudents = (int)(Math.random()*6 + 5);
      String [] studentNames;
      Scanner myScan = new Scanner( System.in );
      studentNames = new String[numStudents];
      for (int i = 0; i < numStudents; i++){
        System.out.print("Enter a student name: ");
        studentNames[i] = myScan.next();
        
        
      }
      
      int [] midtermScores;
      midtermScores = new int[numStudents];
      for (int j = 0; j< numStudents; j++){
        midtermScores[j] = (int)(Math.random()*101);
        System.out.println(studentNames[j]+": " + midtermScores[j]);
      }
    }
}