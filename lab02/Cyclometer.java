/////////////
///CSE 2 Cyclometer
///Luke Christman
///2-2-2018
///This propgram will:
///print the number of minutes for each bicycle trip
///print the number of counts for each trip
///print the distance of each trip in miles
///print the distance for the two trips combined
/////////////
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
// our input data: 
int secsTrip1=480;  //defines the number of seconds of trip 1
int secsTrip2=3220;  //defines the number of seconds of trip 2 
int countsTrip1=1561;  //defines the number of counts of trip 1
int countsTrip2=9037; //defines the number of counts of trip 2

// our intermediate variables and output data
double wheelDiameter=27.0;  //gives the diamter of the wheel in inches which is needed to find distance traveled
double PI=3.14159; //sets PI in order to use it to find the circumfrance of the wheel
double feetPerMile=5280;  //gives the number of feet per mile used for conversions to find distances in miles
double inchesPerFoot=12;   //gives the number of inches in a foot, used to convert inches to feet
double secondsPerMinute=60;  //gives the number of seconds in a minute (60) which will be used to convert the trip times to minutes
  
//print out the output data
  System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had " + countsTrip1+" counts."); //prints the minutes and number of counts in trip 1
	System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts."); //prints the minutes and number of counts in trip 2
       	      
// our calculations to find the distances of each trip and the total distance of both trips, all in miles.
double distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
			//also converts the distance to miles
double distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // does the same, but for trip 2 this time
double totalDistance=distanceTrip1+distanceTrip2; //adds the distances of both trips together to find the total distance

//Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints the number of miles of trip 1
	System.out.println("Trip 2 was "+distanceTrip2+" miles"); // prints the number of miles of trip 2
	System.out.println("The total distance was "+totalDistance+" miles"); //prints the number of miles of trips 1 and 2 combined
	}  //end of main method   
} //end of class