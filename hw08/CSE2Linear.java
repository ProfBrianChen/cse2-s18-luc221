/////////////
///CSE 2 CSE2Linear
///Luke Christman
///4-10-18
///This program will:
///create an array and use different methods to analyze that array, prompting the user to search
///for values within that array. The array will also be scrambled after one search.
/////////////

import java.util.Scanner;//importing scanner


public class CSE2Linear{
   //main method required for all Java programs
    public static void main(String arg[]){
      int [] studentGrades;//declaring the array studentGrades
      int counter = 0;//initiating a counter that will be used to exit the code if the user enters an incorrect input
      studentGrades = new int[15];//alocating a new array that will have 15 values
      Scanner myScan = new Scanner( System.in );//declaring a scanner that will be used to prompt the user for 15 grades
      System.out.println("Please enter 15 integers to represent the grades of the students. Each number must be higher than the previous: ");
      for (int i =0; i<15; i++){//for integers 0-14:

				if ( myScan.hasNextInt()){
					 studentGrades[i] = myScan.nextInt();//assign the users inputted value to that part of the array. This for loop will fill the entire array with integers					
				} 
        else if ( myScan.hasNextInt() == false ){//if the user doesn't enter an integer
          System.out.println("Error: The value entered is not an integer: Please restart the program.");
          //add one to the counter and exit the array
          return;//break out of the for loop
        }
        else if( studentGrades[i]<0 || studentGrades[i]>100 ){//if the student doesn't enter an integer between 0-100
          System.out.println("Error: The value entered is not between 0-100. Please restart the program.");
          // add one to the counter
          return;//break out of the for loop
        }
        if(i>0){//after the user inputs the first value:
          if( studentGrades[i] < studentGrades[i-1] ){//check to make sure every value after that is greater than the previous entered value
            System.out.println("Error: the value entered is not larger than the previous value. Restart the program.");
            return;//break out of the for loop
          }
        }
      }
      for (int i =0; i<15; i++){
        System.out.print(studentGrades[i]+" ");//print out the array in a single line, with spaces in between
      }
    
      binarySearch( studentGrades );//pass the array of studentGrades into the binarySearch method
      System.out.println("Scrambled: ");
      arrayShuffler( studentGrades );//pass the same array into the arrayShuffler method whioch will scramble to array
      
    }    
  
    public static void binarySearch( int[] list ){
			Scanner myScan2 = new Scanner( System.in );//declaring a second scanner that will be used to ask the user for a target grade to search for
      int gradeSearch1 = 0;//declaring gradeSearch1
      System.out.print("Please enter a grade to search for: ");
      gradeSearch1 = myScan2.nextInt();//assigning the input to gradeSearch1
			int counter = 0;//declaring counter outside the loop. This will count how many iterations it takes to find the target grade
      int upperBound = 14;//declaring upper and lower bounds based off of the length of the array
			int lowerBound = 0;
      while (lowerBound <= upperBound){
				int middleVal = (lowerBound+upperBound)/2;//create a value that will be used to access the middle of the array
        if (list[middleVal] == gradeSearch1){//if the array at that middle value is equal to the target grade:
          System.out.println(gradeSearch1 + " was found in " + counter + " iterations."); 
          break;
        }
        else if (list[middleVal] > gradeSearch1){//if the value of the array at the midpoint is greater than the target value
            upperBound = middleVal - 1;//set the upperbound to one lower than the middle 
          }
        else{//if the value of the array is smaller than the target value
          lowerBound = middleVal + 1;//set the lower bound to one above what the middle was
        }
				counter++;//add one to the counter and restart the while loop if they lowerBound is less than the upperBound
      }
			while (lowerBound>upperBound){//when the lower bound surpasses the upper bound, it means the target grade was not found in the array
				System.out.println(gradeSearch1+" was not found in "+ counter + " iterations.");
				break;
			}
			return;//return to main method
    }
  
    public static void linearSearch( int[] list ){
      Scanner myScan2 = new Scanner( System.in );//declare a scanner
			System.out.print("Enter a grade to search for: ");
			int gradeSearch2 = myScan2.nextInt();//set gradeSearch2 to the inputted value
			int counter = 0;//declare a counter outside the loop to count iterations
      for(int i = 0; i<list.length; i++){//creating a for loop that tracks the length of the array
        if (list[i] == gradeSearch2){//if the new array at the value of i is equal to the target grade
          System.out.println(gradeSearch2+ " was found in "+counter+" iterations");//success: print out the number of iterations it took
          break;
					//if the value does not equal the target grade, restart the for loop with the next value in the array
        }
        counter++;//add one to the counter to count interations
				if (i == list.length){//once all values inside the array are checked 
					System.out.println(gradeSearch2 + " was not found in " + counter + " iterations");//let the user know the grade was not found
				}
        }
		return;
      }
      
    
    
    public static void arrayShuffler( int[] list ){
 for (int i=0; i<list.length; i++) {
	//find a random member to swap with
	int target = (int) (list.length * Math.random() );
	 //swap the values
	int scramble = list[target];
	list[target] = list[i];
	list[i] = scramble;
 }
  for (int k = 0; k < list.length; k++){//creating a for loop to access each value within the array
	 int newVal = list[k];//set a variable equal to the array to access each stored quantity in order to print out the new array. This does not change the array
		System.out.print(newVal + " ");
	}
System.out.println();
    
 linearSearch( list );//Now, with the new array titled list, pass this array into the linearSearch method 
    }
    
  
  }
