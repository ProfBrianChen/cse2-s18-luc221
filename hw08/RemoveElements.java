/////////////
///CSE 2 RemoveElements
///Luke Christman
///4-10-18
///This program will:
///create different methods to create an array filled with random integers, delete 
///members of that array based on their position, and remove members of the array base on their values.
/////////////

import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int[] randomInput(){
    int[] randomIntegers;//declares an array called randomIntegers
    randomIntegers = new int[10];//allocates ther length to 10
    for (int i =0; i<10; i++){//accesses each value within the array, 0-9
      randomIntegers[i] = (int)(Math.random()*10);//sets each value within the array to a random integer from 0-9
    }
    return randomIntegers;//returns the array
  }
  
  public static int[] delete(int list[], int pos){
		
    if (pos > list.length || pos<0){//if the value passes into the mathod is greater than the length of the index or less than 0, 
			System.out.println("The integer entered is not valid. It is not within the length of the array.");
      return list;//return the list as is and let the user know they did something wrong
    }
    int[] newList;//declare a new list
    newList = new int[list.length - 1];//set length to one less than the length of the array passed in
    for (int i = 0; i<9; i++){//go through all 10 values of the array
      while (i < pos){//if the entered value that was passed in is equal to i
        newList[i] = list[i];
				break;
      }
			while (i >= pos){
				 newList[i] = list[i+1];
				break;
			} //create a newList equal to that of the original list, minus the one value 
    }
    return newList;  //return the new array  
 }

public static int[] remove(int list[],int target){
	int counter = 0;//declare a counter used to track how many values in the array are the same
	for (int i =0; i<10; i++){//access each value within the array
		if (list[i] == target){//if the value of the array at that i is equal to the target value
			counter++;//add one to the counter
		}//this loop is used to find the length of the new array
	}
	
   int listCheck = 0; //initiating counter outside loop
   int list2Check = 0; //initiating counter
   int[] list2 = new int[list.length - counter];  //allocating new array to the length it would be after all removed values
   while (listCheck < list.length && list2Check < list2.length){  
     if(list[listCheck] != target){//if the value is not equal to the target value:
         list2[list2Check] = list[listCheck]; //set the values of the arrays at the point equal to each other
          listCheck++;//add  one to both counters
          list2Check++;
      }                          
      else {                                           
      	for(int j = listCheck; j < list.length; j++){//creating for loop that will test if there are more than one target values in a row
        	if(list[j] == target){ //if the array at a value equals the target:
           listCheck++; //add one to the counter
        	}
          else{
          j = list.length; //if it does not equal the target value, set j to the length of the list to exit the for loop
          }
        }           
      }
    }
  return list2; // return the new array
}
	
}


