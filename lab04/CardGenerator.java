/////////////
///CSE 2 Card generator
///Luke Christman
///2-4-2018
///This propgram will:
///Generate a random card, including both the identity and suit of the playing card.
///The program will use strings, modulus, if statements, and switches to accomplish this. 
/////////////

public class CardGenerator{
    			// main method required for every Java program
   			public static void main(String[] args) {
  int CardNumber = (int)(Math.random()*51+1); //generates a random number between 1 and 52 as an integer
  String Suit = "";//declaring Suit a string outside the if statement
  String Identity = "";//declaring Idenitity a string outside the if statement
          if ( CardNumber >= 1 && CardNumber <= 13){
             Suit = "diamonds";//designates a fourth of the cards as diamonds
          }
          else if ( CardNumber>13 && CardNumber<=26){
             Suit = "clubs";//designates a fourth of the cards as clubs
          }
          else if (CardNumber>26 && CardNumber<=39){
             Suit = "hearts";//designates a fourth of the cards as hearts
          }
          else if (CardNumber>39 && CardNumber<=52){
             Suit = "spades";//designates a fourth of the cards as spades
          }
          
 int remainder = CardNumber%13;//finds the remainder of dividing 52 by 13
            switch (remainder){
              case 0:
                 Identity = "Ace";//When the remainder is 0, the card is a Ace
                break;
            }
             switch (remainder){
              case 1:
                 Identity = "2";//When the remainder is 1, the card is a 2
                 break;
            }
             switch (remainder){
              case 2:
                 Identity = "3";//When the remainder is 2, the card is a 3
                 break;
            }
             switch (remainder){
              case 3:
                 Identity = "4";//When the remainder is 3, the card is a 4
                 break;
            }
             switch (remainder){
              case 4:
                 Identity = "5";//When the remainder is 4, the card is a 5
                 break;
             }
             switch (remainder){
              case 5:
                 Identity = "6";//When the remainder is 5, the card is a 6
                 break;
        }
     
             switch (remainder){
              case 6:
                 Identity = "7";//When the remainder is 6, the card is a 7
                 break;
        }
   
             switch (remainder){
              case 7:
                 Identity = "8";//When the remainder is 7, the card is a 8
                 break;
        }
   
             switch (remainder){
              case 8:
                 Identity = "9";//When the remainder is 8, the card is a 9
                 break;
        }
                 switch (remainder){
              case 9:
                 Identity = "10";//When the remainder is 9, the card is a 10
                     break;
        }
                 switch (remainder){
              case 10:
                 Identity = "Jack";//When the remainder is 10, the card is a Jack
                     break;
        }
                 switch (remainder){
              case 11:
                 Identity = "Queen";//When the remainder is 11, the card is a Queen
                     break;
        }
                 switch (remainder){
              case 12:
                 Identity = "King";//When the remainder is 12, the card is a King
                     break;
        }
    System.out.println("You picked the " + Identity + " of " + Suit);   // prints the chosen card, including both the suit and idenity     
        
   }
}
         