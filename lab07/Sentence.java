import java.util.Random; // import random class
import java.util.Scanner; // import scanner class

public class Sentence{ // main class

  
  public static String adjective(){
     Random randomGenerator = new Random();
    String adj = "";
    int word1 = randomGenerator.nextInt(10);
    switch (word1){
      case 1: adj = "stupid";
        break;
      case 2: adj = "big";
        break;
      case 3: adj = "large";
        break;
      case 4: adj = "gross";
        break;
      case 5: adj = "handsome";
        break;
      case 6: adj = "marvelous";
        break;
      case 7: adj = "amazing";
        break;
      case 8: adj = "ugly";
        break;
      case 9: adj = "gorgeous";
        break;
    }
    return adj;
  }
 
    public static String subject(){
       Random randomGenerator = new Random();
    String subject = "";
    int word2 = randomGenerator.nextInt(10);;
    switch (word2){
      case 1: subject = "dog";
        break;
      case 2: subject = "human";
        break;
      case 3: subject = "cat";
        break;
      case 4: subject = "car";
        break;
      case 5: subject = "bear";
        break;
      case 6: subject = "artist";
        break;
      case 7: subject = "woman";
        break;
      case 8: subject = "man";
        break;
      case 9: subject = "couch";
        break;
    }
    return subject;
  }
 
     public static String verb(){
        Random randomGenerator = new Random();
    String verb = "";
    int word3 = randomGenerator.nextInt(10);;
    switch (word3){
      case 1: verb = "hit";
        break;
      case 2: verb = "skipped";
        break;
      case 3: verb = "created";
        break;
      case 4: verb = "partied";
        break;
      case 5: verb = "walked";
        break;
      case 6: verb = "moved";
        break;
      case 7: verb = "pushed";
        break;
      case 8: verb = "stole";
        break;
      case 9: verb = "worked";
        break;
    }
    return verb;
  }
 
    public static String object(){
       Random randomGenerator = new Random();
    String object = "";
    int word4 = randomGenerator.nextInt(10);;
    switch (word4){
      case 1: object = "sock";
        break;
      case 2: object = "tree";
        break;
      case 3: object = "ship";
        break;
      case 4: object = "hero";
        break;
      case 5: object = "monkey";
        break;
      case 6: object = "baby";
        break;
      case 7: object = "book";
        break;
      case 8: object = "boyfriend";
        break;
      case 9: object = "exam";
        break;
    }
    return object;
  }
 
       public static String verb2(){
          Random randomGenerator = new Random();
    String verb2 = "";
    int word5 = randomGenerator.nextInt(10);;
    switch (word5){
      case 1: verb2 = "called";
        break;
      case 2: verb2 = "used";
        break;
      case 3: verb2 = "looked";
        break;
      case 4: verb2 = "asked";
        break;
      case 5: verb2 = "swam";
        break;
      case 6: verb2 = "talked";
        break;
      case 7: verb2 = "played";
        break;
      case 8: verb2 = "ate";
        break;
      case 9: verb2 = "jumped";
        break;
    }
    return verb2;
  }
 
 public static void structure1(String subject){
    System.out.println("The " + adjective()+" " +adjective()+" "+subject() +" " +verb()+ " the " + object() + " in school, so it " + verb() + " the " + object() + ".");
    return;
   
  }
 
  public static void structure2(String subject){
    System.out.println("The " + subject + " could be seen as a " + adjective() + " " + object() + ".");
    return;
  }
 
  public static void structure3(String subject){
    System.out.println("It " + verb() + " and " + verb2() + " all day long");
    return;
  }
 
  public static void structure4(String subject){
    System.out.println("The " + object()  + verb2() + " at this " + subject + " with a " + object() + ".");
    return;
  }
 
 
  public static void StoryRun (int sentCount){ // will compile story body
     Random randomGenerator = new Random();
    String sentenceSubject = subject();
    int sentenceStructureCount = 0;
    for (int i = 0; i < sentCount; i++){
      structure1(" ");
      sentenceStructureCount = (int) (Math.random() * 3 + 1);
      switch (sentenceStructureCount){
        case 1: structure2(sentenceSubject);
          break;
        case 2: structure3(sentenceSubject);
          break;
        case 3: structure4(sentenceSubject);
          break;
      }
    }
    System.out.println("");
    System.out.println("That ends the story of the " + adjective() + " " + sentenceSubject + ".");
  }
 
  
  public static void main (String [] args){
    Scanner myScanner = new Scanner (System.in);
    boolean Flag = true;
    int count = 0;
   
    System.out.print("How many sentences would you like to print? Enter a positive integer: ");
    if (myScanner.hasNextInt()){
      count = myScanner.nextInt();
    }
    else {
      myScanner.nextLine();
    }
    if (count < 1){
      Flag = false;
    }
   
    
    while (Flag == false){
      System.out.print("Please input a positive integer for the amount of sentences you would like to print: ");
      if (myScanner.hasNextInt()){
      count = myScanner.nextInt();
      }
      else {
        myScanner.nextLine();
      }
      if (count < 1){
        Flag = false;
      }
      else {
        Flag = true;
      }
    
      
    }
    StoryRun(count);
  }
   
    
}