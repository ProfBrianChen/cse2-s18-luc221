/////////////
///CSE 2 Check
///Luke Christman
///2-4-2018
///This propgram will:
///Calculate the cost per person from eating at a restaurant and takes
///the tip and number of people eating into account.
/////////////

import java.util.Scanner;

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {

Scanner myScanner = new Scanner( System.in ); //tells Scanner that you are creating an instance that will take input from STDIN
System.out.print("Enter the original cost of the check in the form xx.xx: ");//prompts the user to type in the original cost of the check
double checkCost = myScanner.nextDouble();//Tells the program to assign the typed number as a double called checkcost
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");//prompts the user to type in the tip percentage
double tipPercent = myScanner.nextDouble();//Tells the program to assign the typed number as a double called tipPercent
tipPercent /= 100; //We want to convert the percentage into a decimal value
System.out.print("Enter the number of people who went out to dinner: ");//prompts the user to type in the number of people that went to dinner
int numPeople = myScanner.nextInt();//Tells the program to assign the typed number as a int called numPeople
          
double totalCost = checkCost * (1 + tipPercent);//calculates total cost of bill with tip
double costPerPerson = totalCost / numPeople;//calculates the bill price split up between the number of people
int dollars=(int)costPerPerson; //gives the dollar amount of the total cost per person      
int dimes=(int)(costPerPerson * 10) % 10;//gives the first decimal place of the cost per person
int pennies=(int)(costPerPerson * 100) % 10;//gives the second decimal place of the cost per person
          System.out.println("Each person in the group owes $" + (dollars) + (".") + (dimes) + (pennies));        
}  //end of main method   
  	} //end of class